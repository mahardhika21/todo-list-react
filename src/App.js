import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Login from './components/Login/Login';
import Registration from './components/Registration/Registration';
import Header from './components/Header/Header';
import InputItem from './components/InputItem/InputItem';

function App() {
  return (
    <BrowserRouter>
      <Header />
        <Routes>
          <Route path='/' element={<Registration /> } ></Route>
          <Route path='/login' element={<Login />} ></Route>
          <Route path='/todo' element={<InputItem />} ></Route>
        </Routes>
      </BrowserRouter>
  );
}

export default App;
